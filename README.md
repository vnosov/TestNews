TestNews
========

## For ubuntu 16.04.

Install required packages:  
`sudo apt-get install python3 python3-dev python3-pip python3-virtualenv`

Go to the project folder, create a virtual environment and activate it:

`cd ~`  
`git clone https://gitlab.com/vnosov/TestNews`  
`cd TestNews`  
`virtualenv virtualenv -p python3`  
`source ./virtualenv/bin/activate`    

Now install packages required for kivy:  
`pip install -r requirements.txt`  

Install kivy:  
`pip install -r requirements_kivy.txt`    

Run the app:  
`cd src`  
`python main.py`  
