from kivy.lang import Builder
from kivy.uix.label import Label

Builder.load_file('widgets/itemlabel.kv')


class ItemLabel(Label):
    pass
