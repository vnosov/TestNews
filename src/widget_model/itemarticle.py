from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('widgets/itemarticle.kv')


class ItemArticle(BoxLayout):
    article = ObjectProperty()

    def __init__(self, article=None, **kwargs):
        super(ItemArticle, self).__init__(**kwargs)
        self.ids.author.text = article.author
        self.ids.title.text = article.title
        self.ids.url_to_image.source = article.url_to_image
