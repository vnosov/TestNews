from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('widgets/itemboxlayout.kv')


class ItemBoxLayout(BoxLayout):
    data = None
    click_event = False

    def __init__(self, data=None, click_event=False, **kwargs):
        super(ItemBoxLayout, self).__init__(**kwargs)
        self.click_event = click_event
        self.data = data
        """this event dispatches when ItemBoxLayout was clicked
        and click_event is True"""
        self.register_event_type('on_item_box_layout_clicked_event')

    def on_touch_down(self, touch):
        super().on_touch_down(touch)
        if self.click_event:
            if self.collide_point(*touch.pos) and self.data:
                self.dispatch('on_item_box_layout_clicked_event', self.data)

    def on_item_box_layout_clicked_event(self, *args):
        pass
