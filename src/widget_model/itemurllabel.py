import webbrowser

from kivy.lang import Builder
from kivy.properties import StringProperty

from src.widget_model.itemlabel import ItemLabel

Builder.load_file('widgets/itemurllabel.kv')


class ItemUrlLabel(ItemLabel):
    url = StringProperty()

    def __init__(self, url='', **kwargs):
        super(ItemUrlLabel, self).__init__(**kwargs)
        self.url = url

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            webbrowser.open(self.url)
