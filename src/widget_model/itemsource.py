from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('widgets/itemsource.kv')


class ItemSource(BoxLayout):
    source = ObjectProperty()

    def __init__(self, source, **kwargs):
        super(ItemSource, self).__init__(**kwargs)
        self.ids.name.text = source.name
        self.ids.description.text = source.description
