from kivy.lang import Builder
from src.screen_model.basescreen import BaseScreen
from src.widget_model.itemsource import ItemSource
from src.widget_model.itemboxlayout import ItemBoxLayout
from src.widget_model.itemurllabel import ItemUrlLabel

from src.services.newsservice import NewsService

Builder.load_file('screens/sourcelistscreen.kv')


class SourceListScreen(BaseScreen):

    def __init__(self, **kwargs):
        super(SourceListScreen, self).__init__(**kwargs)
        self.sources = []
        self.get_sources()
        # this event dispatches when ItemBoxLayout was clicked
        # and click_event is True
        self.register_event_type('on_item_box_layout_clicked_event')

    def on_enter(self, *args):
        self.ids.main_box.clear_widgets()
        for source in self.sources:
            item_box = ItemBoxLayout(height=130)
            content_item_box = ItemBoxLayout(data=source,
                                             click_event=True)
            content_item_box.add_widget(ItemSource(source=source))
            item_box.add_widget(content_item_box)
            item_box.add_widget(ItemUrlLabel(url=source.url))

            content_item_box.bind(
                on_item_box_layout_clicked_event=self.on_item_box_clicked_event
            )

            self.ids.main_box.add_widget(item_box)
        self.set_scroll_view_height()

    def on_item_box_clicked_event(self, *args):
        self.dispatch('on_item_box_layout_clicked_event', *args)

    def on_item_box_layout_clicked_event(self, *args):
        pass

    def get_sources(self):
        self.sources = NewsService.get_instance().get_sources()

    def set_scroll_view_height(self):
        """
        Calculate height of children in main_box and set height for main_box
        :return: -
        """
        height = 0
        for child in self.ids.main_box.children:
            height += child.height
        self.ids.main_box.height = height
