from kivy.lang import Builder
from kivy.uix.screenmanager import Screen

Builder.load_file('screens/basescreen.kv')


class BaseScreen(Screen):
    pass
