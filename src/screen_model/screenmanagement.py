from kivy.uix.screenmanager import ScreenManager, RiseInTransition

from src.model.artile import Article
from src.model.source import Source
from src.screen_model.articlelistscreen import ArticleListScreen
from src.screen_model.articlescreen import ArticleScreen
from src.screen_model.sourcelistscreen import SourceListScreen


class ScreenManagement(ScreenManager):
    source_list_screen = SourceListScreen()
    article_list_screen = ArticleListScreen()
    article_screen = ArticleScreen()

    def __init__(self, **kwargs):
        super(ScreenManagement, self).__init__(**kwargs)
        self.transition = RiseInTransition()
        self.screens_bind_events()
        self.adding_screens()

    def screens_bind_events(self):
        self.source_list_screen.bind(
            on_item_box_layout_clicked_event=self.open_article_list_screen
        )
        self.article_list_screen.bind(
            on_item_box_layout_clicked_event=self.open_article_screen
        )

    def open_article_list_screen(self, *args):
        for arg in args:
            if isinstance(arg, Source):
                self.article_list_screen.set_source(arg)
                self.to_article_list_screen()
                break

    def open_article_screen(self, *args):
        for arg in args:
            if isinstance(arg, Article):
                self.article_screen.set_article(arg)
                self.to_article_screen()
                break

    def to_source_list_screen(self):
        self.current = 'source_list_screen'

    def to_article_list_screen(self):
        self.current = 'article_list_screen'

    def to_article_screen(self):
        self.current = 'article_screen'

    def adding_screens(self):
        self.add_widget(self.source_list_screen)
        self.add_widget(self.article_list_screen)
        self.add_widget(self.article_screen)
