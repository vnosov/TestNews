from kivy.lang import Builder
from src.screen_model.basescreen import BaseScreen
from src.widget_model.itemarticle import ItemArticle
from src.widget_model.itemboxlayout import ItemBoxLayout

from src.services.newsservice import NewsService

Builder.load_file('screens/articlelistscreen.kv')


class ArticleListScreen(BaseScreen):

    def __init__(self, **kwargs):
        super(ArticleListScreen, self).__init__(**kwargs)
        self.articles = []
        # this event dispatches when ItemBoxLayout was clicked
        # and click_event is True
        self.register_event_type('on_item_box_layout_clicked_event')

    def set_source(self, source):
        self.ids.main_box.clear_widgets()
        news_service = NewsService.get_instance()
        self.articles = news_service.get_articles_by_source(source.id)
        for article in self.articles:
            item_box = ItemBoxLayout(data=article,
                                     click_event=True,
                                     height=500)
            if article.author is None:
                article.author = 'Unknown author'
            item_box.add_widget(ItemArticle(article=article))

            item_box.bind(
                on_item_box_layout_clicked_event=
                self.on_item_box_clicked_event
            )

            self.ids.main_box.add_widget(item_box)
        self.set_scroll_view_height()

    def on_item_box_clicked_event(self, *args):
        self.dispatch('on_item_box_layout_clicked_event', *args)

    def on_item_box_layout_clicked_event(self, *args):
        pass

    def set_scroll_view_height(self):
        """
        Calculate height of children in main_box and set height for main_box
        :return: -
        """
        height = 0
        for child in self.ids.main_box.children:
            height += child.height
        self.ids.main_box.height = height
