from kivy.lang import Builder

from src.screen_model.basescreen import BaseScreen

Builder.load_file('screens/articlescreen.kv')


class ArticleScreen(BaseScreen):

    def __init__(self, **kwargs):
        super(ArticleScreen, self).__init__(**kwargs)

    def set_article(self, article):
        self.ids.author.text = article.author
        self.ids.title.text = article.title
        self.ids.description.text = article.description
        self.ids.url.url = article.url
        self.ids.image.source = article.url_to_image
