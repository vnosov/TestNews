from kivy.app import App
from src.screen_model.screenmanagement import ScreenManagement


class TestNewsApp(App):
    screen_manager = ScreenManagement()

    def build(self):
        self.screen_manager.to_source_list_screen()
        return self.screen_manager


if __name__ == '__main__':
    TestNewsApp().run()
