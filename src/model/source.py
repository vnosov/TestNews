class Source:
    id = ''
    name = ''
    description = ''
    url = ''

    def __init__(self, source_id, name, description, url):
        self.id = source_id
        self.name = name
        self.description = description
        self.url = url
