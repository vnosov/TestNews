import requests

from src.model.artile import Article
from src.model.source import Source
from src.settings import NEWS_API_KEY


class NewsService:
    _instance = None

    def __init__(self):
        self._sources = []
        self._articles = []

    @staticmethod
    def get_instance():
        if NewsService._instance is None:
            NewsService._instance = NewsService()
        return NewsService._instance

    def get_sources(self):
        self._sources.clear()
        news_sources = requests.get(
            'https://newsapi.org/v1/sources?language=en&'
            'source=techcrunch&apiKey={0}'.format(NEWS_API_KEY)
        ).json()['sources']
        for current_source in news_sources:
            source = Source(source_id=current_source['id'],
                            name=current_source['name'],
                            description=current_source['description'],
                            url=current_source['url'])
            self._sources.append(source)
        return self._sources

    def get_articles_by_source(self, source_name=''):
        self._articles.clear()
        if source_name is not '':
            news_articles = requests.get(
                'https://newsapi.org/v1/articles?'
                'source={0}&apiKey={1}'.format(source_name, NEWS_API_KEY)
            ).json()['articles']
            for current_article in news_articles:
                article = Article(author=current_article['author'],
                                  title=current_article['title'],
                                  description=current_article['description'],
                                  url=current_article['url'],
                                  url_to_image=current_article['urlToImage'],
                                  published_at=current_article['publishedAt'])
                self._articles.append(article)
        return self._articles
